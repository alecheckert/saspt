#!/usr/bin/env python
import setuptools
setuptools.setup(
    name="saspt",
    version="1.0",
    packages=setuptools.find_packages(),
    author="Alec Heckert",
    author_email="alecheckert@gmail.com",
    description="State arrays to resolve fast diffusive mixtures from short single particle trajectories"
)
