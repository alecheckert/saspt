# saspt

State arrays to resolve fast diffusive mixtures from short single particle trajectories.

`saspt` is a simple Python package that identifies diffusive subpopulations from noisy single particle tracking (SPT) data. See the [User Guide](https://gitlab.com/alecheckert/saspt/-/blob/main/UserGuide.pdf) for more info.

